﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    public class Channel
    {
        public List<User> users = new List<User>();
        public List<User> ops = new List<User>();
        public string Name { get; set; }
        public string motd { get; set; }
        // OPS list..
    }
}
