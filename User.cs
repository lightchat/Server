﻿using Lidgren.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    public class User
    {
        public long Uniqueidentifier { get; set; }
        public string Nick { get; set; }
        public NetConnection conn { get; set; }
    }
}
