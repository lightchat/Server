﻿using Lidgren.Network;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Server
{
    class Program
    {
        public static NetServer server;
        public static List<NetConnection> connections = new List<NetConnection>();
        public static List<User> users = new List<User>();
        public static List<Channel> channels = new List<Channel>();

        public enum NetMessageType
        {
            ChannelMessage,
            DirectMessage,
            JoinChannel,
            LeaveChannel,
            ChangeName,
            ConfirmNickChange,
            ConfirmChannelJoin,
            ConfirmChannelLeave,
            RequestChannelUsers,
            SendChannelUsers,
            RequestChannelMOTD,
            SendChannelMOTD,
            RequestWHOIS,
            SendWHOIS
        }

        public enum MessageType
        {
            UserChannelMessage,
            UserDirectMessage,
            ConnectMessage,
            DisconnectMessage,
            ChannelConnectMessage,
            ChannelLeaveMessage,
            StatusMessage,
            ChangeName,
            Command,
            NickAlreadyExist
        }

        public enum StatusMessage
        {
            ErrChannelPassword,
            ErrCannotLeave
        }

        public static void updateTitle(string status, int users, int channels)
        {
            Console.Title = "LightChat Server || Status: " + status + " | Users: " + users + " | Channels: " + channels;
        }

        public static bool compare_pass(string pass_hash, string password)
        {
            byte[] hashBytes = Convert.FromBase64String(pass_hash);
            /* Get the salt */
            byte[] salt = new byte[16];
            Array.Copy(hashBytes, 0, salt, 0, 16);
            /* Compute the hash on the password the user entered */
            var pbkdf2 = new Rfc2898DeriveBytes(password, salt, 10000);
            byte[] hash = pbkdf2.GetBytes(20);
            /* Compare the results */
            for (int i = 0; i < 20; i++)
                if (hashBytes[i + 16] != hash[i])
                    return false;

            return true;
        }

        public static void GotMessage(object state)
        {
            NetIncomingMessage msg;
            while ((msg = server.ReadMessage()) != null)
            {
                switch (msg.MessageType)
                {
                    case NetIncomingMessageType.VerboseDebugMessage:
                    case NetIncomingMessageType.DebugMessage:
                    case NetIncomingMessageType.WarningMessage:
                    case NetIncomingMessageType.ErrorMessage:
                        Console.WriteLine(msg.ReadString());
                        break;
                    case NetIncomingMessageType.ConnectionApproval:
                        {
                            msg.SenderConnection.Approve();
                            Console.WriteLine("Accepted connection from: " + msg.SenderConnection.RemoteEndPoint.ToString());
                            connections.Add(msg.SenderConnection);
                            users.Add(new User { Uniqueidentifier = msg.SenderConnection.RemoteUniqueIdentifier, conn = msg.SenderConnection });
                            break;
                        }                        
                    case NetIncomingMessageType.Data:
                        var type = (NetMessageType)msg.ReadByte();

                        switch (type)
                        {
                            case NetMessageType.RequestChannelUsers:
                                {
                                    var channel_name = msg.ReadString();

                                    var user_list = channels.Find(x => x.Name == channel_name).users;

                                    // for now just send name list
                                    string[] name_list = new string[user_list.Count];

                                    int ii = 0;
                                    foreach (var user_name in user_list)
                                    {
                                        name_list[ii] = user_name.Nick;
                                        ii++;
                                    }
                                    List<NetConnection> channel_userss = new List<NetConnection>();
                                    for (int i = 0; i < user_list.Count; i++)
                                    {
                                        channel_userss.Add(user_list[i].conn);
                                    }

                                    if (channel_userss.Count < 1)
                                        return;

                                    NetOutgoingMessage outmsgg = server.CreateMessage();
                                    outmsgg.Write((byte)NetMessageType.RequestChannelUsers);
                                    outmsgg.Write(channel_name);
                                    outmsgg.Write(string.Join(",", name_list));

                                    server.SendMessage(outmsgg, channel_userss, NetDeliveryMethod.ReliableOrdered, 0);
                                    break;
                                }
                            case NetMessageType.RequestChannelMOTD:
                                {
                                    var channel_name = msg.ReadString();
                                    Console.WriteLine("Called request motd");
                                    if (channels.Any(x => x.Name == channel_name))
                                    {
                                        NetOutgoingMessage outmsg = server.CreateMessage();
                                        outmsg.Write((byte)NetMessageType.RequestChannelMOTD);

                                        var requested_channel = channels.Find(x => x.Name == channel_name);
                                        Console.WriteLine("Rquested channel = " + requested_channel.Name);

                                        if (String.IsNullOrWhiteSpace(requested_channel.motd))
                                        {
                                            Console.WriteLine("Sending blank motd");
                                            outmsg.Write(channel_name);
                                            outmsg.Write("");
                                        }
                                        else
                                        {
                                            Console.WriteLine("Sending motd: " + requested_channel.motd);
                                            outmsg.Write(channel_name);
                                            outmsg.Write(requested_channel.motd);
                                        }
                                    }
                                    break;
                                }                                
                            case NetMessageType.ChangeName:
                                {
                                    var nick = msg.ReadString();

                                    if(users.Any(x => x.Nick == nick))
                                    {
                                        NetOutgoingMessage outmsg = server.CreateMessage();
                                        outmsg.Write((byte)MessageType.NickAlreadyExist);
                                        outmsg.Write(nick);
                                        server.SendMessage(outmsg, msg.SenderConnection, NetDeliveryMethod.ReliableOrdered);
                                        return;
                                    }

                                    var last_nick = users.Find(x => x.Uniqueidentifier == msg.SenderConnection.RemoteUniqueIdentifier).Nick;
                                    users.Find(x => x.Uniqueidentifier == msg.SenderConnection.RemoteUniqueIdentifier).Nick = nick;
                                    var user = users.Find(x => x.Uniqueidentifier == msg.SenderConnection.RemoteUniqueIdentifier);

                                    NetOutgoingMessage message = server.CreateMessage();
                                    message.Write((byte)NetMessageType.ConfirmNickChange);
                                    message.Write(nick);
                                    server.SendMessage(message, msg.SenderConnection, NetDeliveryMethod.ReliableOrdered);

                                    var channel_list = channels.FindAll(x => x.users.Any(a => a.Uniqueidentifier == msg.SenderConnection.RemoteUniqueIdentifier) == true);

                                    foreach (var chnll in channels)
                                    {
                                        if (!chnll.users.Any(x => x.Uniqueidentifier == user.Uniqueidentifier))
                                        {
                                            continue;
                                        }

                                        if (chnll.Name == "Status")
                                            continue;

                                        NetOutgoingMessage outmsg = server.CreateMessage();
                                        outmsg.Write((byte)NetMessageType.ChannelMessage);
                                        outmsg.Write((byte)MessageType.ChangeName);
                                        outmsg.Write(chnll.Name);
                                        outmsg.Write(last_nick);
                                        outmsg.Write(nick);

                                        List<NetConnection> chnl_users = new List<NetConnection>();
                                        for (int i = 0; i < chnll.users.Count; i++)
                                        {
                                            chnl_users.Add(chnll.users[i].conn);
                                        }

                                        if (chnl_users.Count < 1)
                                            return;
                                        Console.WriteLine("Send user change");
                                        server.SendMessage(outmsg, chnl_users, NetDeliveryMethod.ReliableOrdered, 0);
                                        sendUserList(chnll.Name);
                                    }
                                    break;
                                }
                            case NetMessageType.JoinChannel:
                                {
                                    var channel = msg.ReadString();

                                    try
                                    {
                                        // "Save" current list, otherwise it can raise errors because it has been changed.
                                        var loc_channels = channels.ToList();

                                        if (loc_channels.Count < 1 || !loc_channels.Any(x => x.Name == channel))
                                        {
                                            var user = users.Find(x => x.Uniqueidentifier == msg.SenderConnection.RemoteUniqueIdentifier);
                                            channels.Add(
                                                new Channel
                                                {
                                                    Name = channel,
                                                });
                                            channels.Find(x => x.Name == channel).users.Add(user);
                                            updateTitle(server.Status.ToString(), users.Count, channels.Count);

                                            NetOutgoingMessage confirm_join_channel = server.CreateMessage();
                                            confirm_join_channel.Write((byte)NetMessageType.ConfirmChannelJoin);
                                            confirm_join_channel.Write(true);
                                            confirm_join_channel.Write(channel);
                                            confirm_join_channel.Write(channels.Find(x => x.Name == channel).motd);
                                            server.SendMessage(confirm_join_channel, msg.SenderConnection, NetDeliveryMethod.ReliableOrdered);

                                            NetOutgoingMessage outsmg = server.CreateMessage();
                                            outsmg.Write((byte)NetMessageType.ChannelMessage);
                                            outsmg.Write((byte)MessageType.ChannelConnectMessage);
                                            outsmg.Write(channel);
                                            outsmg.Write(user.Nick);

                                            List<NetConnection> channel_users = new List<NetConnection>();
                                            for (int i = 0; i < channels.Find(x => x.Name == channel).users.Count; i++)
                                            {
                                                channel_users.Add(channels.Find(x => x.Name == channel).users[i].conn);
                                            }

                                            if (channel_users.Count < 1)
                                                return;

                                            server.SendMessage(outsmg, channel_users, NetDeliveryMethod.ReliableOrdered, 0);
                                            Console.WriteLine(user.Nick + " joined channel " + channel);
                                        }
                                        else
                                        {
                                            var user = users.Find(x => x.Uniqueidentifier == msg.SenderConnection.RemoteUniqueIdentifier);

                                            if (channels.Find(x => x.Name == channel).users.Any(x => x.Uniqueidentifier == msg.SenderConnection.RemoteUniqueIdentifier))
                                                return;

                                            channels.Find(x => x.Name == channel).users.Add(user);

                                            NetOutgoingMessage confirm_join_channel = server.CreateMessage();
                                            confirm_join_channel.Write((byte)NetMessageType.ConfirmChannelJoin);
                                            confirm_join_channel.Write(true);
                                            confirm_join_channel.Write(channel);
                                            server.SendMessage(confirm_join_channel, msg.SenderConnection, NetDeliveryMethod.ReliableOrdered);
                                            
                                            NetOutgoingMessage outsmg = server.CreateMessage();
                                            outsmg.Write((byte)NetMessageType.ChannelMessage);
                                            outsmg.Write((byte)MessageType.ChannelConnectMessage);
                                            outsmg.Write(channel);
                                            outsmg.Write(user.Nick);

                                            List<NetConnection> channel_users = new List<NetConnection>();
                                            for (int i = 0; i < channels.Find(x => x.Name == channel).users.Count; i++)
                                            {
                                                channel_users.Add(channels.Find(x => x.Name == channel).users[i].conn);
                                            }
                                            server.SendMessage(outsmg, channel_users, NetDeliveryMethod.ReliableOrdered, 0);
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine("ERROR: " + e.StackTrace);
                                    }
                                    break;
                                }
                            case NetMessageType.ChannelMessage:
                                {
                                    var channel = msg.ReadString();
                                    var channel_msg = msg.ReadString();
                                    var color = msg.ReadString();

                                    if (String.IsNullOrWhiteSpace(channel_msg))
                                        return;

                                    if (!channels.Any(x => x.Name == channel))
                                        return;

                                    var user_nick = users.Find(x => x.Uniqueidentifier == msg.SenderConnection.RemoteUniqueIdentifier).Nick;
                                    
                                    NetOutgoingMessage outmsg = server.CreateMessage();
                                    outmsg.Write((byte)NetMessageType.ChannelMessage);
                                    outmsg.Write((byte)MessageType.UserChannelMessage);
                                    outmsg.Write(channel);
                                    outmsg.Write(user_nick);
                                    outmsg.Write(channel_msg);

                                    if (!String.IsNullOrEmpty(color))
                                        outmsg.Write(color);

                                    if (channel == "Status")
                                    {
                                        // Handle server-sided commands here.

                                        server.SendMessage(outmsg, msg.SenderConnection, NetDeliveryMethod.ReliableOrdered);
                                        return;
                                    }

                                    List<NetConnection> chnl_users = new List<NetConnection>();
                                    for (int i = 0; i < channels.Find(x => x.Name == channel).users.Count; i++)
                                    {
                                        chnl_users.Add(channels.Find(x => x.Name == channel).users[i].conn);
                                    }

                                    if (chnl_users.Count < 1)
                                        return;

                                    server.SendMessage(outmsg, chnl_users, NetDeliveryMethod.ReliableOrdered, 0);
                                    break;
                                }                                
                            case NetMessageType.DirectMessage:
                                {
                                    var username = msg.ReadString();
                                    var message = msg.ReadString();

                                    var from = users.Find(x => x.Uniqueidentifier ==  msg.SenderConnection.RemoteUniqueIdentifier);
                                    var user = users.Find(x => x.Nick == username);

                                    NetOutgoingMessage outmsg = server.CreateMessage();
                                    outmsg.Write((byte)MessageType.UserDirectMessage);
                                    outmsg.Write(from.Nick);
                                    outmsg.Write(message);

                                    server.SendMessage(outmsg, user.conn, NetDeliveryMethod.ReliableOrdered);
                                    break;
                                }
                            case NetMessageType.LeaveChannel:
                                {
                                    var channel_name = msg.ReadString();

                                    if (channel_name == "Status")
                                    {
                                        NetOutgoingMessage outmsg = server.CreateMessage();

                                        outmsg.Write((byte)NetMessageType.ChannelMessage);
                                        outmsg.Write((byte)MessageType.StatusMessage);
                                        outmsg.Write((byte)StatusMessage.ErrCannotLeave);
                                        server.SendMessage(outmsg, msg.SenderConnection, NetDeliveryMethod.ReliableOrdered);
                                        return;
                                    }

                                    if (channels.Any(x => x.Name == channel_name))
                                    {
                                        var channel_to_leave = channels.Find(x => x.Name == channel_name);
                                        var nick = users.Find(x => x.Uniqueidentifier == msg.SenderConnection.RemoteUniqueIdentifier).Nick;

                                        channel_to_leave.users.RemoveAll(x => x.Uniqueidentifier == msg.SenderConnection.RemoteUniqueIdentifier);

                                        
                                        NetOutgoingMessage outmsg = server.CreateMessage();

                                        outmsg.Write((byte)NetMessageType.ConfirmChannelLeave);
                                        outmsg.Write(true);
                                        outmsg.Write(channel_name);
                                        server.SendMessage(outmsg, msg.SenderConnection, NetDeliveryMethod.ReliableOrdered);

                                        sendLeave(nick, channel_name);

                                        if(channel_name != "Status")
                                        {
                                            sendUserList(channel_name);
                                        }
                                    }
                                    else
                                    {
                                        NetOutgoingMessage outmsg = server.CreateMessage();

                                        outmsg.Write((byte)NetMessageType.ConfirmChannelLeave);
                                        outmsg.Write(false);
                                        outmsg.Write(channel_name);
                                        server.SendMessage(outmsg, msg.SenderConnection, NetDeliveryMethod.ReliableOrdered);
                                    }
                                    break;
                                }                                
                            case NetMessageType.ConfirmNickChange:
                                break;
                            case NetMessageType.ConfirmChannelJoin:
                                break;
                            default:
                                break;
                        }
                        break;
                    case NetIncomingMessageType.StatusChanged:
                        NetConnectionStatus status = (NetConnectionStatus)msg.ReadByte();

                        if(status == NetConnectionStatus.Disconnected)
                        {
                            // Find the user that disconnected
                            var usr = users.Find(x => x.Uniqueidentifier == msg.SenderConnection.RemoteUniqueIdentifier);
                            // Some logging
                            Console.WriteLine("User disconnected (Nick, IP): " + usr.Nick + ", " + usr.conn.RemoteEndPoint.Address);

                            // Remove the user from the lists
                            connections.RemoveAll(x => x.RemoteUniqueIdentifier == usr.conn.RemoteUniqueIdentifier);
                            users.RemoveAll(x => x.Uniqueidentifier == usr.conn.RemoteUniqueIdentifier);

                            // Remove the user from the channels it where, and send a disconnect message.
                            foreach (var chnll in channels)
                            {
                                if(!chnll.users.Any(x => x.Nick == usr.Nick))
                                {
                                    continue;
                                }

                                chnll.users.RemoveAll(x => x.Uniqueidentifier == usr.Uniqueidentifier);

                                if (chnll.Name == "Status")
                                    continue;

                                sendUserList(chnll.Name);

                                NetOutgoingMessage outmsg = server.CreateMessage();
                                outmsg.Write((byte)NetMessageType.ChannelMessage);
                                outmsg.Write((byte)MessageType.DisconnectMessage);
                                outmsg.Write(chnll.Name);
                                outmsg.Write(usr.Nick);

                                if (channels.Find(x => x.Name == chnll.Name).users.Count < 1)
                                    continue;

                                List<NetConnection> chnl_users = new List<NetConnection>();
                                for (int i = 0; i < channels.Find(x => x.Name == chnll.Name).users.Count; i++)
                                {
                                    chnl_users.Add(channels.Find(x => x.Name == chnll.Name).users[i].conn);
                                }

                                if (chnl_users.Count < 1)
                                    continue;

                                updateTitle(server.Status.ToString(), users.Count, channels.Count);
                                server.SendMessage(outmsg, chnl_users, NetDeliveryMethod.ReliableOrdered, 0);
                            }
                        }

                        if(status == NetConnectionStatus.Connected)
                        {
                            if(channels.Count < 1 || !channels.Any(x => x.Name == "Status"))
                            {
                                channels.Add(
                                        new Channel
                                        {
                                            Name = "Status"
                                        });

                                channels.Find(x => x.Name == "Status").users.Add(users.Find(x => x.Uniqueidentifier == msg.SenderConnection.RemoteUniqueIdentifier));
                                NetOutgoingMessage join_status_msg = server.CreateMessage();
                                join_status_msg.Write((byte)NetMessageType.ConfirmChannelJoin);
                                join_status_msg.Write(true);
                                join_status_msg.Write("Status");
                                server.SendMessage(join_status_msg, msg.SenderConnection, NetDeliveryMethod.ReliableOrdered);
                                return;
                            }

                            if (channels.Find(x => x.Name == "Status").users.Any(x => x.Uniqueidentifier == msg.SenderConnection.RemoteUniqueIdentifier))
                                return;

                            channels.Find(x => x.Name == "Status").users.Add(users.Find(x => x.Uniqueidentifier == msg.SenderConnection.RemoteUniqueIdentifier));
                            NetOutgoingMessage status_join_msg = server.CreateMessage();
                            status_join_msg.Write((byte)NetMessageType.ConfirmChannelJoin);
                            status_join_msg.Write(true);
                            status_join_msg.Write("Status");
                            server.SendMessage(status_join_msg, msg.SenderConnection, NetDeliveryMethod.ReliableOrdered);

                            updateTitle(server.Status.ToString(), users.Count, channels.Count);
                        }
                        break;
                    default:
                        Console.WriteLine("Unhandled type: " + msg.MessageType);
                        break;
                }
                server.Recycle(msg);
            }
        }

        public static void sendUserList(string channel_name)
        {
            var user_list = channels.Find(x => x.Name == channel_name).users;

            // for now just send name list
            string[] name_list = new string[user_list.Count];

            int ii = 0;
            foreach (var user_name in user_list)
            {
                name_list[ii] = user_name.Nick;
                ii++;
            }
            List<NetConnection> channel_userss = new List<NetConnection>();
            for (int i = 0; i < user_list.Count; i++)
            {
                channel_userss.Add(user_list[i].conn);
            }

            if (channel_userss.Count < 1)
                return;

            NetOutgoingMessage outmsgg = server.CreateMessage();
            outmsgg.Write((byte)NetMessageType.RequestChannelUsers);
            outmsgg.Write(channel_name);
            outmsgg.Write(string.Join(",", name_list));

            server.SendMessage(outmsgg, channel_userss, NetDeliveryMethod.ReliableOrdered, 0);
        }

        public static void sendLeave(string nick, string channel)
        {
            NetOutgoingMessage outmsg = server.CreateMessage();
            outmsg.Write((byte)NetMessageType.ChannelMessage);
            outmsg.Write((byte)MessageType.ChannelLeaveMessage);
            outmsg.Write(channel);
            outmsg.Write(nick);

            List<NetConnection> chnl_users = new List<NetConnection>();
            for (int i = 0; i < channels.Find(x => x.Name == channel).users.Count; i++)
            {
                chnl_users.Add(channels.Find(x => x.Name == channel).users[i].conn);
            }

            if (chnl_users.Count < 1)
                return;

            server.SendMessage(outmsg, chnl_users, NetDeliveryMethod.ReliableOrdered, 0);
        }

        static void Main(string[] args)
        {
            SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());

            NetPeerConfiguration config = new NetPeerConfiguration("LightChat");
            

            config.EnableMessageType(NetIncomingMessageType.ConnectionApproval);
            config.EnableMessageType(NetIncomingMessageType.Data);
            config.EnableMessageType(NetIncomingMessageType.VerboseDebugMessage);
            config.EnableMessageType(NetIncomingMessageType.Error);
            config.EnableMessageType(NetIncomingMessageType.ErrorMessage);
            config.EnableMessageType(NetIncomingMessageType.DebugMessage);
            config.EnableMessageType(NetIncomingMessageType.WarningMessage);

            config.Port = 21002;

            server = new NetServer(config);
            server.Start();
            updateTitle(server.Status.ToString(), 0, 0); // load db?
            Console.WriteLine("Started server... ");
            server.RegisterReceivedCallback(new SendOrPostCallback(GotMessage));
            while (true) ;
        }
    }
}
